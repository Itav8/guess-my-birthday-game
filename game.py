from random import randint

# random_numbers_year = randint(1924, 2004)
# random_numbers_month = randint(1, 12)

# guessing_year = random_numbers_year
# guessing_month = random_numbers_month

name = input("Hi, what's is your name? ")

for guess_num in range(1, 6):
    random_numbers_year = randint(1924, 2004)
    random_numbers_month = randint(1, 12)

    guessing_year = random_numbers_year
    guessing_month = random_numbers_month
    guesses = print("Guess", guess_num, name, ": were you born in", int(guessing_month), "/", int(guessing_year), "?")

    response = input("yes or no? ")

    if response == "yes":
        print('I knew it!')
        exit()
    elif guess_num == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
